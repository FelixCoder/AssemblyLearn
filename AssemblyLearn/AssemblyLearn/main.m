//
//  main.m
//  AssemblyLearn
//
//  Created by FelixYin on 2018/5/27.
//  Copyright © 2018年 FelixYin. All rights reserved.
//

#import <Foundation/Foundation.h>

int test(int a,int b){
    return (a+b);
}

int main(int argc, const char * argv[]) {
    
    test(2, 1);
    
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
    }
    return 0;
}
